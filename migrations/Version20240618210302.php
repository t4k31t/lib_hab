<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240618210302 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE author (
            guid uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(), 
            author_name VARCHAR NOT NULL, 
            book_count INT NOT NULL, 
            created_at TIMESTAMP not null default current_timestamp, 
            updated_at TIMESTAMP not null default current_timestamp
            )');

        $this->addSql('CREATE TABLE book (
            guid uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(), 
            book_name VARCHAR(255) NOT NULL, 
            description VARCHAR(255) NOT NULL, 
            publish_date DATE NOT NULL, 
            created_at TIMESTAMP not null default current_timestamp, 
            updated_at TIMESTAMP not null default current_timestamp
            )');

        $this->addSql('CREATE TABLE authors_books (
            book_id UUID NOT NULL, FOREIGN KEY (book_id) REFERENCES book(guid) NOT DEFERRABLE INITIALLY IMMEDIATE, 
            author_id UUID NOT NULL, FOREIGN KEY (author_id) REFERENCES author(guid) NOT DEFERRABLE INITIALLY IMMEDIATE
            )');

        $this->addSql('CREATE TABLE "user" (
            guid uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(), 
            user_name VARCHAR(255) NOT NULL, 
            email VARCHAR(255) DEFAULT NULL, 
            password VARCHAR(255) DEFAULT NULL, 
            token INT DEFAULT NULL, 
            verified BOOLEAN default false, 
            created_at TIMESTAMP not null default current_timestamp, 
            updated_at TIMESTAMP not null default current_timestamp
            )');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64924A232CF ON "user" (user_name)');
        $this->addSql('ALTER TABLE authors_books ADD CONSTRAINT FK_2DFDA3CB16A2B381 FOREIGN KEY (book_id) REFERENCES book (guid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE authors_books ADD CONSTRAINT FK_2DFDA3CBF675F31B FOREIGN KEY (author_id) REFERENCES author (guid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BDAFD8C8BACCF6D9 ON author (author_name)');
        $this->addSql('CREATE INDEX IDX_2DFDA3CB16A2B381 ON authors_books (book_id)');
        $this->addSql('CREATE INDEX IDX_2DFDA3CBF675F31B ON authors_books (author_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE authors_books DROP CONSTRAINT FK_2DFDA3CB16A2B381');
        $this->addSql('ALTER TABLE authors_books DROP CONSTRAINT FK_2DFDA3CBF675F31B');
        $this->addSql('DROP TABLE author');
        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE authors_books');
        $this->addSql('DROP TABLE "user"');
    }
}
