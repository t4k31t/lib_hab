<?php

namespace App\Controller;

use App\Exception\BadRequestException;
use App\Services\AuthorServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class AuthorController extends AbstractController
{
    public function __construct(
        private readonly AuthorServices $authorServices
    ) {
    }

    /**
     * @throws BadRequestException
     */
    #[Route('/getAuthorInfo', name: 'api_author', methods: ['get'])]
    public function getAuthorInfo(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $guid = $data['guid'] ?? null;
        $authorName = $data['authorName'] ?? null;
        $author = $this->authorServices->getAuthorInfo($guid, $authorName);

        return new JsonResponse(['success' => $author], 200);
    }
}
