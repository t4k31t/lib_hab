<?php

namespace App\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TitleController extends AbstractController
{
    #[Route('/', name: 'title')]
    public function index(): Response
    {
        return $this->render('title.html.twig');
    }
}
