<?php

//
// namespace App\Controller;
//
// use App\Exception\DuplicateException;
// use App\Services\RegistrationServices;
// use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// use Symfony\Component\HttpFoundation\JsonResponse;
// use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\Routing\Attribute\Route;
//
// class RegistrationController extends AbstractController
// {
//    public function __construct(
//        private RegistrationServices $registrationServices
//    )
//    {}
//
//    /**
//     * @throws DuplicateException
//     */
//    #[Route('/registration', name: 'api_register', methods: ['POST'])]
//    public function registration(Request $request, ):JsonResponse
//    {
//        $data = json_decode($request->getContent(), true);
//        $name = $data['name'];
//        $email = $data['email'];
// //        $password = $data['password'];
//        try{
//        $this->registrationServices->registration($name, $email);}
//        catch (DuplicateException $e){
//            return new JsonResponse($e->getMessage(), $e->getCode());
//        }
//        return new JsonResponse(['success' => true], 200);
//    }
// }
