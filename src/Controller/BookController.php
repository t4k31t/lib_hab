<?php

namespace App\Controller;

use App\Exception\NotFoundException;
use App\Services\BookServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class BookController extends AbstractController
{
    public function __construct(
        private readonly BookServices $bookServices
    ) {
    }

    #[Route('/createBook', name: 'api_book', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $description = $data['description'];
        $bookTitle = $data['bookTitle'];
        $publishDate = $data['publishDate'];
        $authors = $data['authors'];
        $this->bookServices->create($description, $bookTitle, $publishDate, $authors);

        return new JsonResponse(['success' => true], 200);
    }

    /**
     * @throws NotFoundException
     */
    #[Route('/updateBook', name: 'api_update', methods: ['PUT'])]
    public function update(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $guid = $data['guid'];
        $description = $data['description'] ?? null;
        $bookName = $data['bookName'] ?? null;
        $publishDate = $data['publishDate'] ?? null;
        $authors = $data['authors'] ?? null;
        $currentAuthors = $data['currentAuthors'] ?? null;
        $this->bookServices->updateBook($guid, $description, $bookName, $publishDate, $currentAuthors, $authors);

        return new JsonResponse(['success' => true], 200);
    }

    /**
     * @throws NotFoundException
     */
    #[Route('/deleteBook', name: 'api_delete', methods: ['DELETE'])]
    public function delete(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $guid = $data['guid'];
        $this->bookServices->deleteBook($guid);

        return new JsonResponse(['success' => true], 200);
    }

    #[Route('/getBookInfo', name: 'api_get', methods: ['GET'])]
    public function getBookInfo(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $guid = $data['guid'];
        $book = $this->bookServices->getBookInfo($guid);

        return new JsonResponse(['success' => $book], 200);
    }
}
