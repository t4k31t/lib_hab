<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;

#[ORM\Entity(repositoryClass: BookRepository::class)]
#[ORM\Table(name: 'book')]
#[ORM\HasLifecycleCallbacks]
class Book
{
    #[Id]
    #[GeneratedValue(strategy: 'CUSTOM')]
    #[CustomIdGenerator(class: UuidGenerator::class)]
    #[Column(type: Types::GUID, unique: true, nullable: false)]
    private string $guid;
    #[Column(type: 'string', nullable: false)]
    private string $bookName;

    /** Owning Side */
    #[ManyToMany(targetEntity: Author::class, inversedBy: 'book', cascade: ['persist'])]
    #[JoinTable(name: 'authors_books')]
    #[JoinColumn(name: 'book_id', referencedColumnName: 'guid')]
    #[InverseJoinColumn(name: 'author_id', referencedColumnName: 'guid')]
    private Collection $authors;
    #[Column(type: 'string', nullable: false)]
    private string $description;
    #[Column(type: 'date', nullable: false)]
    private \DateTime $publishDate;
    #[Column(type: 'datetime')]
    private ?\DateTime $createdAt = null;
    #[Column(type: 'datetime', nullable: false)]
    private \DateTime $updatedAt;

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getBookName(): string
    {
        return $this->bookName;
    }

    public function setBookName(string $name): self
    {
        $this->bookName = $name;

        return $this;
    }

    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    public function addAuthor(Author $author): self
    {
        $this->authors[] = $author;

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        $this->authors->removeElement($author);

        return $this;
    }

    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPublishDate(): \DateTime
    {
        return $this->publishDate;
    }

    public function setPublishDate(\DateTime $publishDate): self
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    #[ORM\PreFlush]
    public function preFlush(): void
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }
}
