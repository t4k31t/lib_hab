<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Book>
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    /**
     * @throws NonUniqueResultException
     */

    public function duplicateBook(string $bookName, array $authors): bool
    {
        $book = $this->findOneBy(['bookName' => $bookName]);
        if (null === $book) {
            return true;
        }
        $authorsNames = array_map('strtolower', $authors);
        $bookAuthors = $book->getAuthors();
        foreach ($bookAuthors as $author) {
            $key = array_search(strtolower($author->getAuthorName()), $authorsNames);
            if (false !== $key) {
                unset($authorsNames[$key]);
            }
        }
        if (0 === count($authorsNames)) {
            return false;
        }

        return true;
    }
}
