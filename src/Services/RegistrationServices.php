<?php

namespace App\Services;

use App\Entity\User;
use App\Exception\DuplicateException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

readonly class RegistrationServices
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        //        private UserPasswordHasherInterface $passwordHasher,
    ) {
    }

    public function registration(string $name): User
    {
        $userRepository = $this->entityManager->getRepository(User::class);
        $suer = $userRepository->findOneBy(['email' => $name]);
        if (null !== $suer) {
            throw new DuplicateException();
        }
        $user = new User();
        $user
            ->setUserName($name);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
