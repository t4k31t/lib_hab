<?php

namespace App\Services;

use App\Entity\Author;
use App\Entity\Book;
use App\Exception\DuplicateException;
use App\Exception\NotFoundException;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;

class BookServices
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private BookRepository $bookRepository,
    ) {
    }

    /**
     * @throws \Exception
     */
    /**
     * @param array<mixed, author> $authors
     *
     * @throws DuplicateException
     */
    public function create(string $description, string $bookName, string $publishDate, array $authors): Book
    {
        if (!$this->bookRepository->duplicateBook($bookName, $authors)) {
            throw new DuplicateException();
        }
        $publishDateObject = new \DateTime($publishDate);
        $book = new Book();
        $book
            ->setDescription($description)
            ->setBookName($bookName)
            ->setPublishDate($publishDateObject);
        foreach ($authors as $authorName) {
            $authorEntity = $this->entityManager->getRepository(Author::class)->findOneBy(['authorName' => $authorName]);
            if (null === $authorEntity) {
                $authorEntity = new Author();
                $authorEntity
                    ->setAuthorName($authorName)
                    ->addBook($book)
                    ->setBookCount(1);
                $book->addAuthor($authorEntity);
                $this->entityManager->persist($authorEntity);
            } else {
                $book->addAuthor($authorEntity);
                $authorEntity->setBookCount($authorEntity->getBookCount() + 1);
                $this->entityManager->persist($authorEntity);
            }
        }

        $this->entityManager->persist($book);
        $this->entityManager->flush();

        return $book;
    }

    /**
     * @throws NotFoundException
     * @throws \Exception
     */
    public function updateBook(string $guid, ?string $description, ?string $bookName, string $publishDate, ?array $currentAuthors, ?array $authors): Book
    {
        $book = $this->entityManager->getRepository(Book::class)->findOneBy(['guid' => $guid]);
        if (null === $book) {
            throw new NotFoundException();
        }
        if (null !== $description) {
            $book->setDescription($description);
        }
        if (null !== $bookName) {
            $book->setBookName($bookName);
        }
        if (null !== $publishDate) {
            $publishDateObject = new \DateTime($publishDate);
            $book->setPublishDate($publishDateObject);
        }
        if (null !== $authors) {
            foreach ($authors as $author) {
                $authorEntity = $this->entityManager->getRepository(Author::class)->findOneBy(['authorName' => $author]);
                if (null !== $authorEntity) {
                    throw new DuplicateException();
                }
                $authorEntity = new Author();
                $authorEntity
                    ->setAuthorName($author)
                    ->setBookCount(1);
                $book->addAuthor($authorEntity);
            }
        }
        if (null !== $currentAuthors) {
            foreach ($currentAuthors as $author) {
                $authorEntity = $this->entityManager->getRepository(Author::class)->findOneBy(['authorName' => $author]);
                if (null !== $authorEntity) {
                    $authorEntity->setBookCount($authorEntity->getBookCount() - 1);
                    $book->removeAuthor($authorEntity);
                } else {
                    throw new NotFoundException();
                }
            }
        }
        $this->entityManager->persist($book);
        $this->entityManager->flush();

        return $book;
    }

    /**
     * @throws NotFoundException
     */
    public function deleteBook(string $guid): bool
    {
        $book = $this->entityManager->getRepository(Book::class)->findOneBy(['guid' => $guid]);
        if (null === $book) {
            throw new NotFoundException();
        }
        $authors = $book->getAuthors();
        foreach ($authors as $author) {
            $author->setBookCount($author->getBookCount() - 1);
        }
        $this->entityManager->remove($book);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @throws NotFoundException
     */
    public function getBookInfo(string $guid): array
    {
        $book = $this->entityManager->getRepository(Book::class)->findOneBy(['guid' => $guid]);
        if (null === $book) {
            throw new NotFoundException();
        }
        $authorArray = $book->getAuthors();
        $authors = [];
        foreach ($authorArray as $author) {
            $authors[] = $author->getAuthorName();
        }
        $bookData = [
            'guid' => $book->getGuid(),
            'description' => $book->getDescription(),
            'bookName' => $book->getBookName(),
            'publishDate' => $book->getPublishDate()->format('Y-m-d'),
            'authors' => $authors,
        ];

        return $bookData;
    }
}
