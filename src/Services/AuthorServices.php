<?php

namespace App\Services;

use App\Entity\Author;
use App\Exception\BadRequestException;
use Doctrine\ORM\EntityManagerInterface;

class AuthorServices
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @throws BadRequestException
     */

    public function getAuthorInfo(?string $guid, ?string $authorName): array
    {
        if (null === $guid && null === $authorName) {
            throw new BadRequestException();
        }
        switch (empty($guid)) {
            case true:
                $author = $this->entityManager->getRepository(Author::class)->findOneBy(['authorName' => $authorName]);
                break;
            case false:
                $author = $this->entityManager->getRepository(Author::class)->findOneBy(['guid' => $guid]);
                break;
        }
        if (null === $author) {
            throw new BadRequestException();
        }

        $booksCollection = $author->getBooks();
        $bookArray = [];
        foreach ($booksCollection as $books) {
            $bookArray[] = $books->getBookName();
        }
        $authorData = [
            'guid' => $author->getGuid(),
            'authorName' => $author->getAuthorName(),
            'bookCount' => $author->getBookCount(),
            'books' => $bookArray,
        ];

        return $authorData;
    }
}
