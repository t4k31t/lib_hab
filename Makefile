php:
	docker compose exec php bash

migrate:
	docker compose exec php bash -c "php bin/console doctrine:migrations:migrate"